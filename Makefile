#!/usr/bin/make -f

KERNELRELEASE	?= $(shell uname -r)
KERNEL_DIR	?= /lib/modules/$(KERNELRELEASE)/build
PWD		:= $(shell pwd)

obj-m		:= avm-job.o
MODULE_OPTIONS	=

#CFLAGS_avm-job.o := -DDEBUG

.PHONY: all
all: avm-job.ko
avm-job.ko: avm-job.c
	@echo "Building avm-job driver..."
	$(MAKE) -C $(KERNEL_DIR) M=$(PWD) modules

.PHONY: install
install:
	$(MAKE) -C $(KERNEL_DIR) M=$(PWD) modules_install

.PHONY: clean
clean:
	rm -f *~
	rm -f Module.symvers Module.markers modules.order
	$(MAKE) -C $(KERNEL_DIR) M=$(PWD) clean
	rm -rf initrd
	rm -f initrd.gz

.PHONY: distclean
distclean: clean

.PHONY: modprobe
modprobe: avm-job.ko
	chmod a+r $<
	-sudo rmmod avm-job
	sudo insmod ./$< $(MODULE_OPTIONS)

.PHONY: test
test: avm-job.ko
	./run-test
