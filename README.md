Linux Kernel Module
===================

Aufgabe
-------

Es soll ein dynamisch ladbares Linux Kernel Modul entwickelt werden, welches gegen die externe Kernel-Buildschnittstelle gebaut wird und die folgenden Interfaces für den Userspace bereitstellt.

Dabei sollen die folgenden Kernel APIs genutzt werden Workqueue/Timer, Liste, Mutex/Semaphor.
1. Beim Laden/Entladen des Moduls soll eine entsprechende Log Nachricht im Kernel Log auftauchen
2. Datenaustausch mit dem Userspace: Es soll ein Interface angelegt werden welches:
   1. Text-Daten vom Userspace annimmt und in einem *dynamischen* internen Speicher ablegt
   2. Text-Daten aus dem internen Speicher an den Userspace zurückgibt
3. Benutzung der Kernel API:
   1. Die Text-Daten aus den internen Speicher sollen regelmässig, 1 Wort pro Sekunde, in das Kernel Log geschrieben werden

Das Ergebnis soll als git-Repo mit Source Datei und Kernel Log bereitgestellt werden.
Dabei hast Du freie Auswahl des Linux Kernel Releases (4.9.x, 5.4.x, 5.18.x, etc) und der Zielumgebung (Standard-Linux-Desktop, Emuliertes oder Eingebettettes System).

Falls es Dein erstes Kernel Modul ist, ist [LDD3](https://lwn.net/Kernel/LDD3/) ein guter Einstieg.

Anmerkungen
-----------

Ich habe das Modul [avm-job.c](avm-job.c) under Debian 12 Bookworm mit Linux Kernel 6.1.90-1 entwickelt und getestet.
Das [Makefile](Makefile) baut per `make all` das Modul, sofern die Linux-Kernel-Quellen (genauer: die Header-Dateien) unter `/lib/modules/$(uname -r)/build` verlinkt sind. Unter Debian wird das durch das Paket `linux-headers-amd64` zur Verfügung gestellt.

Sofern auf dem (Test-)System auch [Qemu/KVM](https://www.qemu.org/) kann über den Aufruf von `make test` ein Test gestartet werden:
1. Dieses verwendet den Linux-Kernel der Host-Systems wieder.
2. Baut aber eine eigene minimale "Initiale RAM-Disk"  mit `busybos`, dem Kernel-Modul und dem Test-Skript [run-test](run-test).
3. Dieses lädt innerhalb von Qemu dann das Modul und führt einige grundlegende Tests aus:
   1. Laden eines einfachen Textes "I like FritzOS!" in das Modul und anschließendes auslesen: Testest `open()`, `write()` und `read()`.
   2. Laden des Texts "Welcome at AVM" in 3 Schritten und erneutes auslesen: Testet das aneinanderhängen von kleinen `writes()`.
   3. Anhängen des weiteres Textes "Greetings from Oldenburg" und auslesen des Gesamttextes: Testet `O_APPEND`-Verhalten.
   4. Überschreibt alle vorherigen Texte mit "Hello from Berlin": Testet `O_TRUNCATE`.
   5. Schreibt mehrere Worte mit verschiedenen Trennzeichen (" \r\n\t\f"): Testet das Aufspalten in Worte. Siehe [Bersonderheiten](#Besonderheiten).
   6. Schreiben von 1 MiB in einem Block: Testet große Datenmengen.
   7. Schreiben von 1 MiB als 1024× 1 KiB: Testet abermals große Datenmengen.
   8. Modul entladen und erneut laden: Testet (De-)Initialisirung.

```console
$ make test
Building avm-job driver...
make -C /lib/modules/6.1.0-21-amd64/build M=/home/pmhahn/prog/AVM/JobLinuxModule modules
make[1]: Verzeichnis „/usr/src/linux-headers-6.1.0-21-amd64“ wird betreten
  CC [M]  /home/pmhahn/prog/AVM/JobLinuxModule/avm-job.o
  MODPOST /home/pmhahn/prog/AVM/JobLinuxModule/Module.symvers
  LD [M]  /home/pmhahn/prog/AVM/JobLinuxModule/avm-job.ko
  BTF [M] /home/pmhahn/prog/AVM/JobLinuxModule/avm-job.ko
Skipping BTF generation for /home/pmhahn/prog/AVM/JobLinuxModule/avm-job.ko due to unavailability of vmlinux
make[1]: Verzeichnis „/usr/src/linux-headers-6.1.0-21-amd64“ wird verlassen
./run-test
+ /bin/insmod /avm-job.ko
[    0.426605] avm_job: loading out-of-tree module taints kernel.
[    0.427142] avm_job: module verification failed: signature and/or required key missing - tainting kernel
[    0.428091] AVM job module loaded
+ sleep 1
+ awk '$2=="misc" {print $1}' /proc/devices
+ major=10
+ awk '$2=="avm-job" {print $1}' /proc/misc
+ minor=124
+ mknod /dev/avm-job c 10 124
+ ls -l /dev/avm-job
crw-r--r--    1 0        0          10, 124 May 23 22:10 /dev/avm-job
+ echo 'I like FritzOS!'
+ sleep 1
+ cat /dev/avm-job
I like FritzOS!
+ sleep 1
[    2.461591] avm-timer: I
+ exec
+ printf Wel
+ printf 'come at '
+ printf 'AVM!\n'
+ exec
+ cat /dev/avm-job
Welcome at AVM!
+ sleep 1
[    3.485597] avm-timer: Welcome
+ echo 'Greetings from Oldenburg'
+ cat /dev/avm-job
Welcome at AVM!
Greetings from Oldenburg
+ sleep 6
[    4.510374] avm-timer: at
[    5.534160] avm-timer: AVM!
[    6.557731] avm-timer: Greetings
[    7.582159] avm-timer: from
[    8.606162] avm-timer: Oldenburg
+ echo 'Hello from Berlin'
+ cat /dev/avm-job
Hello from Berlin
+ sleep 4
[   10.654159] avm-timer: Hello
[   11.677586] avm-timer: from
[   12.702158] avm-timer: Berlin
+ exec
+ printf Fritz
+ sleep 2
[   14.749597] avm-timer: Fritz
+ printf 'OS!\tby\rAVM\ffrom\nBerlin'
+ sleep 6
[   16.798159] avm-timer: OS!
[   17.822172] avm-timer: by
[   18.846396] avm-timer: AVM
[   19.870159] avm-timer: from
[   20.894163] avm-timer: Berlin
+ exec
+ cat /dev/avm-job
AVMtzOS!        by
   from
Berlin+ cat /dev/avm-job
AVMtzOS!        by
   from
Berlin+ sleep 1
+ dd 'if=/dev/zero' 'of=/dev/avm-job' 'bs=1M' 'count=1' 'status=none'
+ wc -c
+ '[' 1048576 -eq 1048576 ]
+ dd 'if=/dev/zero' 'of=/dev/avm-job' 'bs=1K' 'count=1K' 'status=none'
+ wc -c
+ '[' 1048576 -eq 1048576 ]
+ /bin/rmmod avm-job
[   23.496590] Unloading AVM job module
+ sleep 1
+ /bin/insmod /avm-job.ko
[   24.508684] AVM job module loaded
+ /bin/rmmod avm-job
[   24.510621] Unloading AVM job module
+ sleep 1
+ /bin/poweroff -n -f
[   25.525597] ACPI: PM: Preparing to enter system sleep state S5
[   25.526672] reboot: Power down
```
   
### Besonderheiten

#### Timer vs. chunked writes

Für den Aufgabenteil 3.1 habe ich mich für folgende Implementierung entschieden:
- Das Modul merkt sich intern in der Variablen `word_pos` den Beginn des nächsten Worts.
- Führende Leerzeichen `[ \0\t\f\r\n]` werden ignoriert und übersprungen.
- Alles bis zum nächsten Leerzeichen wird dann als ein Wort interpretiert und ausgegeben.
- Verkompliziert wird das durch ein Nebenläufigkeitsproblem:
  1. Schreibt ein Prozess erst "A", wartet dann so lange, bis das nach 1s ausgegeben wurde, und dann erst "B", wird dieses "B" nach einer weiteren Sekunde dann auch ausgegeben. Im Speicher steht "AB".
  2. Schreibt der Prozess aber direkt "AB", steht dieses so auch im Speicher, aber der Timer gibt nur eine Meldung für "AB" aus.

  D.h. in beiden Fällen steht im Speicher "AB", aber es können 1 oder 2 Meldungen erscheinen.
  Weitere Spezialfälle entstehen dann dadurch, das vorherige Schreibvorgänge mit Leerzeichen aufhören bzw. nachfolgende damit anfangen.

#### Timer reset

Aus der Aufgabenstellung für 3.1 geht nicht klar hervor, ab wann das 1s-Intervall starten soll:
1. entweder ab dem letzten Schreibvorgang?
2. soll ein Überschreiben den Timer neu startet?

Ich bin auf dieses Problem in meiner Implementierung nicht näher eingegangen und lasse den Timer einfach im Intervall von 1s weiterlaufen, selbst wenn keine weiteren Daten mehr zur Aushabe vorliegen.
Man könnte (und sollte?) den Timer dann besser anhalten, um CPU-Ressourcen zu sparen.
Falls das noch gewünscht ist, kann ich das noch nachliefern.

#### Misc device

Ich habe mich entschieden, den Treiber als "Miscellaneous Character Device" zu implementieren.
Lauf Aufgabenstellung ist nur ein "Device" und damit nur eine Minor-Nummer notwendig.
Der Verzicht auf dynamische Allocation vereinfacht die Implementierung.
