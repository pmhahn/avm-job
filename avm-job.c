#include <linux/init.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/fs.h>
#include <linux/miscdevice.h>
#include <linux/errno.h>
#include <linux/list.h>
#include <linux/slab.h>
#include <linux/minmax.h>
#include <linux/mutex.h>
#include <linux/sched.h>

static long delay = 1000;
module_param(delay, long, S_IRUGO);
MODULE_PARM_DESC(delay, "Delay between words printed to kernel log in ms");

static size_t total;
static loff_t word_pos;
struct chunk {
	struct list_head list;
	size_t size;
	char buf[0];
};

static LIST_HEAD(content);
static DEFINE_MUTEX(atomic);

struct timer_list avm_timer;

static loff_t avm_llseek(struct file *filp, loff_t off, int whence) {
	loff_t newpos;

	pr_debug("avm_llseek()\n");
	mutex_lock(&atomic);
	switch (whence) {
		case SEEK_SET:
			newpos = off;
			break;
		case SEEK_CUR:
			newpos = filp->f_pos + off;
			break;
		case SEEK_END:
			newpos = total + off;
			break;
		default:
			newpos = -EINVAL;
			goto out;
	}
	if (newpos < 0) {
		newpos = -EINVAL;
		goto out;
	}
	filp->f_pos = newpos;
out:
	mutex_unlock(&atomic);
	return newpos;
}

static struct chunk *find_chunk(loff_t *offset) {
	struct chunk *chunk;
	list_for_each_entry(chunk, &content, list) {
		if (*offset < chunk->size) {
			return chunk;
		}
		*offset -= chunk->size;
	}
	return NULL;
}

static ssize_t avm_read(struct file *filp, char __user *buff, size_t count, loff_t *f_pos) {
	ssize_t result = 0;

	pr_debug("avm_read(%zd @ %lld)\n", count, *f_pos);
	mutex_lock(&atomic);

	loff_t offset = *f_pos;
	if (offset >= total) {  // read past EOF
		goto out;
	}
	if (offset + count > total) {  // remaining tail
		count = total - offset;
	}

	struct chunk *chunk = find_chunk(&offset);
	size_t chunk_remaining = chunk->size - offset;
	size_t chunk_size = min_t(size_t, count, chunk_remaining);
	if (copy_to_user(buff, chunk->buf + offset, chunk_size)) {
		result = -EFAULT;
		goto out;
	}
	count = chunk_size; // partial read is okay
	*f_pos += count;
	result = count;

out:
	pr_debug("avm_read(%zd) -> %zd\n", count, result);
	mutex_unlock(&atomic);
	return result;
}

static ssize_t avm_write(struct file *filp, const char __user *buff, size_t count, loff_t *f_pos) {
	ssize_t result = 0;

	pr_debug("avm_write(%zd @ %lld)\n", count, *f_pos);
	mutex_lock(&atomic);

	/* given write offset or end for O_APPEND */
	loff_t offset = (filp->f_flags & O_APPEND) ? (loff_t) total : *f_pos;

	struct chunk *chunk = find_chunk(&offset);
	size_t write_remaining = count;
	while (write_remaining > 0 && chunk) {
		size_t chunk_remaining = chunk->size - offset;
		size_t chunk_size = min_t(size_t, write_remaining, chunk_remaining);
		if (copy_from_user(chunk->buf + offset, buff, chunk_size)) {
			result = -EFAULT;
			goto out;
		}
		offset -= chunk_size;
		write_remaining -= chunk_size;
		buff += chunk_size;
		chunk = list_next_entry(chunk, list);
	}
	if (write_remaining > 0) {
		struct chunk *new = kmalloc(sizeof(struct chunk) + offset + write_remaining, GFP_KERNEL);
		if (!new) {
			result = -ENOMEM;
			goto out;
		}
		memset(new->buf, 0, offset);  // write hole after EOF
		if (copy_from_user(new->buf + offset, buff, write_remaining)) {
			kfree(new);
			result = -EFAULT;
			goto out;
		}
		new->size = write_remaining;
		list_add_tail(&new->list, &content);
		total += write_remaining;
	}
	*f_pos += count;
	result = count;

out:
	pr_debug("avm_write(%zd) -> %zd\n", count, result);
	mutex_unlock(&atomic);
	return result;
}

static void truncate(void) {
	struct chunk *chunk, *tmp;
	list_for_each_entry_safe(chunk, tmp, &content, list) {
		list_del(&chunk->list);
		kfree(chunk);
	}
	total = 0;
	word_pos = 0;
}

static int avm_open(struct inode *ino, struct file *filp) {
	pr_debug("avm_open(%05x) %s %s\n", filp->f_flags, filp->f_flags & O_TRUNC ? "TRUNC" : "", filp->f_flags & O_APPEND ? "APP" : "");
	mutex_lock(&atomic);
	if ((filp->f_mode & FMODE_WRITE) && (filp->f_flags & O_TRUNC)) {
		truncate();
	}
	mutex_unlock(&atomic);
	return 0;
}

static int avm_close(struct inode *ino, struct file *filp) {
	pr_debug("avm_close()\n");
	return 0;
}

static bool is_word_separator(const char c) {
	switch (c) {
		case '\0':
		case '\t':
		case '\f':
		case '\r':
		case '\n':
		case ' ':
			return true;
		default:
			return false;
	}
}

static void avm_timer_handler(struct timer_list *t) {
	pr_debug("avm-timer()\n");
	mutex_lock(&atomic);
	if (word_pos >= total) {
		goto out;  // EOF
	}
	// strip leading separators
	loff_t s_off = word_pos, s_pos = word_pos;
	struct chunk *s_chunk = find_chunk(&s_off);
	for (; s_pos < total && is_word_separator(s_chunk->buf[s_off]); s_pos++) {
		if (++s_off >= s_chunk->size) {
			s_chunk = list_next_entry(s_chunk, list);
			s_off = 0;
		}
	}
	if (s_pos >= total) {
		goto out;  // only trailing whitespace left until EOF
	}
	// find next word separator
	loff_t e_off = s_off, e_pos = s_pos;
	struct chunk *e_chunk = s_chunk;
	for (; e_pos < total && !is_word_separator(e_chunk->buf[e_off]); e_pos++) {
		if (++e_off >= e_chunk->size) {
			e_chunk = list_next_entry(e_chunk, list);
			e_off = 0;
		}
	}
	// allocate output buffer
	size_t len = 1 + e_pos - s_pos;
	char *buf = kmalloc(len, GFP_KERNEL), *c;
	if (!buf) {
		pr_err("Failed to allocate temporary buffer of size %zd\n", len);
		goto out;
	}
	// copy word into buffer
	for (c = buf; s_pos < total && !is_word_separator(s_chunk->buf[s_off]); s_pos++) {
		*c++ = s_chunk->buf[s_off];
		if (++s_off >= s_chunk->size) {
			s_chunk = list_next_entry(s_chunk, list);
			s_off = 0;
		}
	}
	*c = '\0';
	pr_info("avm-timer: %s\n", buf);
	word_pos = s_pos;
	kfree(buf);
out:
	mutex_unlock(&atomic);
	mod_timer(&avm_timer, jiffies + msecs_to_jiffies(delay));
}

static struct file_operations avm_fops = {
	.owner = THIS_MODULE,
	.llseek = avm_llseek,
	.read = avm_read,
	.write = avm_write,
	.open = avm_open,
	.release = avm_close,
};

static struct miscdevice avm_dev = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = "avm-job",
	.fops = &avm_fops,
};

static int __init avm_job_init(void) {
	int result;

	if ((result = misc_register(&avm_dev)) < 0) {
		pr_err("Failed to register misc device: %d\n", result);
		return result;
	};

	timer_setup(&avm_timer, avm_timer_handler, 0);
	mod_timer(&avm_timer, jiffies + msecs_to_jiffies(delay));

	pr_info("AVM job module loaded\n");
	return 0;
}

static void __exit avm_job_exit(void) {
	pr_info("Unloading AVM job module\n");
	del_timer_sync(&avm_timer);
	truncate();
	misc_deregister(&avm_dev);
}

module_init(avm_job_init);
module_exit(avm_job_exit);

MODULE_DESCRIPTION("AVM job module");
MODULE_AUTHOR("Philipp Hahn <pmhahn@pmhahn.de>");
MODULE_LICENSE("GPL");
MODULE_VERSION("1.0.0");
